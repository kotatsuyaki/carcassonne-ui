import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

class GameoverPage extends StatefulWidget {
  static const routeName = '/gameover';

  @override
  State<StatefulWidget> createState() => GameoverPageState();
}

class GameoverPageState extends State<GameoverPage> {
  @override
  Widget build(BuildContext context) {
    final Shader linearGradient = LinearGradient(
      colors: <Color>[
        Colors.red,
        Colors.orange,
        Colors.green,
        Colors.blue,
        Colors.purple,
      ],
    ).createShader(Rect.fromLTWH(300.0, 0.0, 300.0, 70.0));

    return Scaffold(
      body: Center(
        child: Text(
          'YOU LOSE',
          style: GoogleFonts.blackOpsOne(
              textStyle: TextStyle(
            fontSize: 80.0,
            fontWeight: FontWeight.bold,
            foreground: Paint()..shader = linearGradient,
          )),
        ),
      ),
    );
  }
}
