import 'dart:collection';
import 'dart:math';

import 'package:carcassonne/palette.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' hide Action;
import 'package:flutter/gestures.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../generated/carcassonne.pb.dart';
import '../models/gameplay_model.dart';
import '../widgets/list_extensions.dart';
import '../game_utils.dart';
import './cover_page.dart';

const defaultFilterQuality = FilterQuality.medium;
const tileSize = 128;
const mapSize = 72 * tileSize * 2;

class GameplayPage extends StatefulWidget {
  static const String routeName = '/gameplay';

  final GameplayPageArguments args;
  GameplayPage({required this.args});

  @override
  State<StatefulWidget> createState() => GameplayPageState();
}

class GameplayPageState extends State<GameplayPage> {
  bool _gameStarted = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade100,
      body: ChangeNotifierProvider(
        create: (context) => GameplayModel.makeDefault(
          roomId: widget.args.roomId,
          me: widget.args.me,
          token: widget.args.token,
          onGameStart: () => this._gameStarted = true,
          onConnectionError: this._handleConnectionError,
        ),
        builder: (context, _child) {
          return Consumer<GameplayModel>(builder: (context, model, _child) {
            if (this._gameStarted)
              return _gameplayView(context, widget.args, gameplayModel: model);
            else
              return _waitingView(context, widget.args, gameplayModel: model);
          });
        },
      ),
    );
  }

  Widget _waitingView(
    BuildContext context,
    GameplayPageArguments args, {
    required GameplayModel gameplayModel,
  }) {
    return Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            width: 400,
            child: Text(
              '(room ${args.roomId} serial ${gameplayModel.state?.serial}) Waiting for players to join ...',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          SizedBox(
            width: 400,
            child: LinearProgressIndicator(),
          ),
        ].mapPadding(EdgeInsets.symmetric(vertical: 8)),
      ),
    );
  }

  Widget _gameplayView(
    BuildContext context,
    GameplayPageArguments args, {
    required GameplayModel gameplayModel,
  }) {
    final screenSize = MediaQuery.of(context).size;

    return Stack(
      children: [
        Positioned.fill(
          child: GameCanvasView(
            screenSize: screenSize,
          ),
        ),
        Positioned(
          top: 8,
          right: 8,
          child: PlayerInfoList(),
        ),
        Positioned(
          right: 8,
          bottom: 8,
          child: Column(
            children: [
              DrawnTileView(),
              SizedBox(height: 8),
              DeckButton(
                onPressed: () => _handleDeckButtonPressed(gameplayModel),
                remainingTiles: gameplayModel.state?.board.deck.length ?? 0,
                deck: gameplayModel.state?.board.deck ?? [],
              ),
            ],
          ),
        ),
        Positioned(
          top: 8,
          left: 8,
          child: _exitButtonWidget(gameplayModel)
              .tooltipped('Exit game', verticalOffset: null),
        ),
      ],
    );
  }

  IconButton _exitButtonWidget(GameplayModel gameplayModel) {
    return IconButton(
      icon: Transform(
        alignment: Alignment.center,
        transform: Matrix4.rotationY(pi),
        child: Icon(Icons.logout),
      ),
      onPressed: () => this._handleExitPressed(gameplayModel),
    );
  }

  void _handleConnectionError({
    required String message,
    required GameplayModel model,
    required bool disconnected,
  }) {
    final messenger = ScaffoldMessenger.of(context);
    messenger.removeCurrentSnackBar();
    messenger.showSnackBar(
      SnackBar(
        duration: Duration(days: 365),
        width: 280,
        behavior: SnackBarBehavior.floating,
        content: Text(message),
        action: disconnected
            ? SnackBarAction(
                label: 'RECONNECT',
                onPressed: () => this._handleRetryPressed(model),
              )
            : null,
      ),
    );
  }

  void _handleRetryPressed(GameplayModel model) {
    model.startStateLoop();
  }

  void _handleExitPressed(GameplayModel model) {
    model.stopStateLoop();
    Navigator.of(context).popUntil(
      (route) => route.settings.name == CoverPage.routeName,
    );
  }

  void _handleDeckButtonPressed(GameplayModel model) {
    Navigator.of(context).push(
      HeroDialogRoute(
        fullscreenDialog: true,
        builder: (_) => DeckModal(model: model),
      ),
    );
  }
}

class DeckButton extends StatelessWidget {
  final void Function()? onPressed;
  final int remainingTiles;
  final List<TileKind> deck;

  const DeckButton({
    required this.onPressed,
    required this.remainingTiles,
    required this.deck,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Hero(
        flightShuttleBuilder:
            (context, animation, direction, fromContext, toContext) {
          return AnimatedBuilder(
            animation: animation,
            builder: (context, widget) => Card(
              color: ColorTween(
                begin: Palette.blacks.shade50,
                end: Colors.grey.shade50,
              ).evaluate(animation),
              shadowColor: ColorTween(
                begin: Palette.blacks.shade50,
                end: Colors.black,
              ).evaluate(animation),
            ),
          );
        },
        tag: 'deck',
        child: ElevatedButton(
          onPressed: this.onPressed,
          style: ElevatedButton.styleFrom(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
          ),
          child: Row(
            children: [
              Text('$remainingTiles TILES'),
              SizedBox(width: 4),
              Icon(
                Icons.arrow_forward_rounded,
                size: 18,
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          ),
        ),
      ),
      width: 144,
    );
  }
}

class GameCanvasView extends StatefulWidget {
  final Size screenSize;

  const GameCanvasView({required this.screenSize});

  @override
  GameCanvasViewState createState() => GameCanvasViewState();
}

class GameCanvasViewState extends State<GameCanvasView>
    with TickerProviderStateMixin {
  final _transformationController = TransformationController();

  // Calculate offset for tile
  static Offset _offsetFor(Coord coord) => Offset(
        coord.j * (tileSize + 2) + mapSize / 2,
        coord.i * (tileSize + 2) + mapSize / 2,
      );

  bool _isScaledDown = false;

  @override
  void initState() {
    super.initState();

    // Navigate to center of map initially
    setState(() {
      this._transformationController.value = Matrix4.inverted(Matrix4.identity()
        ..translate(
          (mapSize.toDouble() - widget.screenSize.width + tileSize) / 2,
          (mapSize.toDouble() - widget.screenSize.height + tileSize) / 2,
        ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return InteractiveViewer(
      scaleEnabled: true,
      panEnabled: true,
      transformationController: this._transformationController,
      onInteractionUpdate: this._handleInteractionUpdate,
      constrained: false,
      minScale: 0.1,
      maxScale: 10.0,
      child: SizedBox(
        width: mapSize.toDouble(),
        height: mapSize.toDouble(),
        child: Consumer<GameplayModel>(builder: (context, gameplayModel, _) {
          return Stack(
            children: [
              _dismissBackground(gameplayModel),
              _placedTiles(gameplayModel)
                  .dimmed(gameplayModel.selectedCoord != null),
              if (gameplayModel.state?.board.turn == gameplayModel.me)
                _candidateTiles(gameplayModel),
            ],
          );
        }),
      ),
    );
  }

  Widget _dismissBackground(GameplayModel model) {
    return GestureDetector(
      onTap: () => this._handleDismissSelection(model),
    );
  }

  Widget _placedTiles(GameplayModel model) {
    final placedTileWidgets = <Widget>[];

    if (model.state != null) {
      final state = model.state!;

      for (final entry in state.board.tiles) {
        final offset = _offsetFor(entry.coord);

        placedTileWidgets.add(Positioned(
          left: offset.dx,
          top: offset.dy,
          width: tileSize.toDouble(),
          height: tileSize.toDouble(),
          child: PlacedTile(
            entry: entry,
            tileSize: tileSize.toDouble(),
          ),
        ));
      }
    }

    return RepaintBoundary(
      child: Stack(
        children: placedTileWidgets,
      ),
    );
  }

  Widget _candidateTiles(GameplayModel model) {
    final candidateTileWidgets = <Widget>[];

    // only render if there's a drawn tile
    if (model.state != null && model.state!.board.deck.isNotEmpty) {
      final state = model.state!;

      // get drawn card asset
      final drawnAssetPath = 'assets/tiles/${state.board.deck.last.name}.png';
      final drawnTileKind = state.board.deck.last;

      // classify actions first
      final actionsByCoord = HashMap<Coord, List<Action>>();
      for (final action in state.legalActions) {
        final coord = action.coord;
        actionsByCoord.putIfAbsent(coord, () => []);
        actionsByCoord[coord]!.add(action);
      }

      // add candidate tiles
      actionsByCoord.forEach((coord, actions) {
        final offset = _offsetFor(coord);
        candidateTileWidgets.add(Positioned(
          left: offset.dx,
          top: offset.dy,
          width: tileSize.toDouble(),
          height: tileSize.toDouble(),
          child: CandidateTile(
            kind: drawnTileKind,
            candidateAssetPath: drawnAssetPath,
            legalActions: actions,
            tileSize: tileSize.toDouble(),
            selected: model.selectedCoord == coord,
            me: model.me,
          ),
        ));
      });
    }

    return RepaintBoundary(
      child: Stack(
        children: candidateTileWidgets,
      ),
    );
  }

  void _handleInteractionUpdate(ScaleUpdateDetails details) {
    print('Viewer scale: ${details.scale}');

    if (details.scale < 1 && this._isScaledDown == false) {
      setState(() {
        _isScaledDown = true;
      });
    } else if (details.scale >= 1 && this._isScaledDown == true) {
      setState(() {
        _isScaledDown = false;
      });
    }
  }

  void _handleDismissSelection(GameplayModel model) {
    model.unsetSelectedCoord();
  }
}

class DrawnTileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<GameplayModel>(builder: (context, gameplayModel, _) {
      if (gameplayModel.isMyTurn &&
          gameplayModel.state?.board.deck.isNotEmpty == true)
        return SizedBox(
          child: Material(
            child: Image(
              image: gameplayModel.state!.board.deck.last.asset(),
              filterQuality: defaultFilterQuality,
            ).tooltipped('Drawn card', verticalOffset: 76),
            borderRadius: BorderRadius.all(Radius.circular(8)),
            elevation: 4.0,
            clipBehavior: Clip.antiAlias,
          ),
          width: 144,
        );
      else
        return SizedBox();
    });
  }
}

class PlayerInfoList extends StatelessWidget {
  const PlayerInfoList();

  @override
  Widget build(BuildContext context) {
    return Consumer<GameplayModel>(
      builder: (context, gameplayModel, _) {
        final scores = gameplayModel.scores ?? [0, 0];
        final meeplesRemaining = gameplayModel.meeplesRemaining ?? [0, 0];

        return Column(
          children: [
            PlayerInfoCard(
              color: Colors.red,
              name: gameplayModel.state?.playerNames[0] ?? 'Player 1',
              score: scores[0],
              meeplesRemaining: meeplesRemaining[0],
              isPlayersTurn: gameplayModel.state?.board.turn == Player.Red,
              meepleAssetPath: 'assets/misc/MeepleRed.png',
            ),
            PlayerInfoCard(
              color: Colors.blue,
              name: gameplayModel.state?.playerNames[1] ?? 'Player 2',
              score: scores[1],
              meeplesRemaining: meeplesRemaining[1],
              isPlayersTurn: gameplayModel.state?.board.turn == Player.Blue,
              meepleAssetPath: 'assets/misc/MeepleBlue.png',
            ),
          ],
        );
      },
    );
  }
}

class PlayerInfoCard extends StatelessWidget {
  final Color color;
  final String name;
  final int score;
  final int meeplesRemaining;
  final bool isPlayersTurn;
  final String meepleAssetPath;

  PlayerInfoCard({
    required this.color,
    required this.name,
    required this.score,
    required this.meeplesRemaining,
    required this.isPlayersTurn,
    required this.meepleAssetPath,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: Card(
        child: Padding(
          child: Column(
            children: [
              _headRow(context),
              _meeplesRow(context),
            ],
          ),
          padding: const EdgeInsets.all(8.0),
        ),
        clipBehavior: Clip.antiAliasWithSaveLayer,
        elevation: this.isPlayersTurn ? 4.0 : null,
      ),
      width: 240,
    );
  }

  /// Row showing player's metadata
  Row _headRow(BuildContext context) {
    return Row(
      children: [
        // Bar and score
        Container(
          child: Text(
            '${this.score}',
            style: Theme.of(context).textTheme.headline4,
          ),
          decoration: BoxDecoration(
            border: Border(
              left: BorderSide(color: this.color, width: 4),
            ),
          ),
          padding: EdgeInsets.only(left: 12),
        ),
        // Player name
        Row(
          children: [
            if (this.isPlayersTurn)
              SpinKitPulse(
                duration: Duration(milliseconds: 800),
                color: Colors.black,
                size: Theme.of(context).textTheme.subtitle1!.fontSize!,
              ),
            if (this.isPlayersTurn) SizedBox(width: 8),
            Text(
              this.name,
              style: Theme.of(context)
                  .textTheme
                  .subtitle1
                  ?.apply(color: Colors.black.withAlpha(0x8a)),
            ),
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
        ),
      ].mapPadding(EdgeInsets.all(8)),
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }

  /// Row showing remaining meeples
  Padding _meeplesRow(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Image(
            image: AssetImage(this.meepleAssetPath),
            filterQuality: defaultFilterQuality,
            width: 24,
            height: 24,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              '×${this.meeplesRemaining}',
              style: Theme.of(context).textTheme.headline6,
            ),
          )
        ],
      ),
    );
  }
}

class PlacedTile extends StatelessWidget {
  final TileEntry entry;
  final double tileSize;

  const PlacedTile({
    required this.entry,
    required this.tileSize,
  });

  static const _meepleSize = 36.0;

  @override
  Widget build(BuildContext context) {
    final tile = entry.tile;
    final meeple = entry.hasMeeple() ? entry.meeple : null;

    final rotation = tile.rotation.value.toDouble();
    final angle = rotation / 2 * -pi;

    return Stack(
      children: [
        Transform.rotate(
          child: ClipRRect(
            child: Image(
              image: tile.kind.asset(),
              filterQuality: defaultFilterQuality,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          angle: angle,
        ),
        if (meeple != null) _meepleWidget(tile, meeple),
      ],
    );
  }

  Widget _meepleWidget(Tile tile, Meeple meeple) {
    final topLeft = TileDataRegistry.meepleTopLeftFor(
      tile: tile,
      action: meeple.meepleAction,
      tileSize: tileSize,
      meepleSize: _meepleSize,
    );
    return Positioned(
      child: Padding(
        child: Image(
          image: meeple.player.meepleAsset(),
          filterQuality: defaultFilterQuality,
        ),
        padding: const EdgeInsets.all(4.0),
      ),
      left: topLeft.x,
      top: topLeft.y,
      width: _meepleSize,
      height: _meepleSize,
    );
  }
}

class CandidateTile extends StatefulWidget {
  final String? candidateAssetPath;
  final bool selected;
  final double tileSize;
  final List<Action> legalActions;
  final List<Rotation> _legalRotations;
  final TileKind kind;
  final Player me;

  CandidateTile({
    required this.candidateAssetPath,
    required this.legalActions,
    required this.tileSize,
    required this.selected,
    required this.kind,
    required this.me,
  }) : _legalRotations = _createLegalRotations(legalActions);

  Coord get _coord => this.legalActions[0].coord;

  static List<Rotation> _createLegalRotations(List<Action> legalActions) {
    final rotations = HashSet<Rotation>();
    for (final action in legalActions) {
      rotations.add(action.rotation);
    }
    return rotations.toList();
  }

  @override
  State<StatefulWidget> createState() => CandidateTileState();
}

class CandidateTileState extends State<CandidateTile>
    with SingleTickerProviderStateMixin {
  /// Current index into [widget._legalRotations]
  int _rotationIndex = 0;

  bool _hovered = false;
  double _scale = 0.8;
  late AnimationController _scaleController;

  Rotation get _rotation => widget._legalRotations[_rotationIndex];

  @override
  void initState() {
    super.initState();

    this._scaleController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 2),
      lowerBound: 0.8,
      upperBound: 0.95,
      value: 0.8,
    );
    this._scaleController.addListener(() {
      setState(() {
        this._scale = this._scaleController.value;
      });
    });
  }

  @override
  void didUpdateWidget(covariant CandidateTile oldWidget) {
    super.didUpdateWidget(oldWidget);

    // Reset some states on widget change
    if (listEquals(oldWidget.legalActions, widget.legalActions) == false) {
      this._rotationIndex = 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: MouseRegion(
        child: Transform.scale(
          child: (widget.selected || this._hovered)
              ? this._tileWidget()
              : this._placeholderWidget(),
          // don't scale down when the tile is selected
          scale: widget.selected ? 1 : this._scale,
        ),
        onEnter: this._handleMouseEnter,
        onExit: this._handleMouseExit,
      ),
      onTap: () => this._handleTap(context),
      onSecondaryTap: this._handleSecondaryTap,
    ).tooltipped(
      widget.selected ? 'Click again to skip meeple placement' : null,
    );
  }

  Widget _tileWidget() {
    final rotation = widget._legalRotations[_rotationIndex].value.toDouble();
    final angle = rotation / 2 * -pi;

    return Stack(
      children: [
        Transform.rotate(
          child: ClipRRect(
            child: Image(
              image: AssetImage(widget.candidateAssetPath ?? _unknownAssetPath),
              filterQuality: defaultFilterQuality,
            ),
            borderRadius: BorderRadius.circular(8),
          ),
          angle: angle,
        ),
        if (widget.selected) this._meepleCandidatesWidget()
      ],
    );
  }

  static const _meepleSize = 36.0;

  Widget _meepleCandidatesWidget() {
    final actions = widget.legalActions
        .where((action) => action.rotation == this._rotation);
    final buttons = <Widget>[];

    for (final action in actions) {
      if (action.placesMeeple) {
        final topLeft = TileDataRegistry.meepleTopLeftFor(
          tile: Tile(kind: widget.kind, rotation: this._rotation),
          action: action.meepleAction,
          tileSize: widget.tileSize,
          meepleSize: _meepleSize,
        );

        buttons.add(Positioned(
          left: topLeft.x,
          top: topLeft.y,
          width: _meepleSize,
          height: _meepleSize,
          child: MeepleButton(
            onTap: () => this._handleMeepleTap(context, action),
            player: widget.me,
          ),
        ));
      }
    }

    return Stack(children: buttons);
  }

  Widget _placeholderWidget() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Image(
        image: AssetImage(_defaultAssetPath),
        filterQuality: defaultFilterQuality,
      ),
    );
  }

  static const _defaultAssetPath = 'assets/tiles/Null0.png';
  static const _unknownAssetPath = 'assets/tiles/Unknown.png';

  void _handleMeepleTap(BuildContext context, Action action) {
    final model = Provider.of<GameplayModel>(context, listen: false);
    model.doAction(action);
  }

  void _handleTap(BuildContext context) {
    print('Major click on coord ${widget._coord.i}, ${widget._coord.j}');

    final model = Provider.of<GameplayModel>(context, listen: false);
    if (widget.selected) {
      final action = widget.legalActions
          .where((action) => action.rotation == _rotation)
          .where((action) => action.placesMeeple == false)
          .first;
      model.doAction(action);
    } else {
      model.setSelectedCoord(widget._coord);
    }
  }

  void _handleSecondaryTap() {
    print('secondary');
    final newIndex = (this._rotationIndex + 1) % widget._legalRotations.length;
    setState(() {
      this._rotationIndex = newIndex;
    });
  }

  void _handleMouseEnter(PointerEnterEvent event) {
    this._scaleController.fling();
    setState(() {
      _hovered = true;
    });
  }

  void _handleMouseExit(PointerExitEvent event) {
    this._scaleController.fling(velocity: -1.0);
    setState(() {
      _hovered = false;
    });
  }
}

class MeepleButton extends StatefulWidget {
  final void Function()? onTap;
  final Player player;

  MeepleButton({
    required this.onTap,
    required this.player,
  });

  @override
  State<StatefulWidget> createState() => MeepleButtonState();
}

class MeepleButtonState extends State<MeepleButton>
    with SingleTickerProviderStateMixin {
  double _padding = 4;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: _handleEnter,
      onExit: _handleExit,
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: this._handleTap,
        child: AnimatedPadding(
          duration: const Duration(milliseconds: 150),
          padding: EdgeInsets.all(_padding),
          child: Image(
            image: widget.player.meepleAsset(),
            filterQuality: defaultFilterQuality,
          ),
        ),
      ),
    );
  }

  void _handleTap() {
    widget.onTap?.call();
  }

  void _handleEnter(PointerEnterEvent event) {
    setState(() {
      _padding = 0;
    });
  }

  void _handleExit(PointerExitEvent event) {
    setState(() {
      _padding = 4;
    });
  }
}

class DeckModal extends StatelessWidget {
  final GameplayModel model;

  DeckModal({required this.model});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: _buildWithConstraints,
    );
  }

  Widget _buildWithConstraints(
    BuildContext context,
    BoxConstraints constraints,
  ) {
    final width = min(constraints.maxWidth * 0.9, 800.0);
    final height = min(constraints.maxHeight * 0.9, 600.0);

    return Center(
      child: SizedBox(
        child: Hero(
          tag: 'deck',
          child: Card(
            child: ChangeNotifierProvider.value(
                value: model,
                child: Consumer<GameplayModel>(builder: (context, model, _) {
                  return _grid(context, model);
                })),
            color: Colors.grey.shade50,
            elevation: 4,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(4.0)),
            ),
          ),
        ),
        width: width,
        height: height,
      ),
    );
  }

  Widget _grid(BuildContext context, GameplayModel model) {
    final deck = model.state?.board.deck ?? [];
    final tileCounts = HashMap<TileKind, int>();

    for (final kind in deck) {
      tileCounts.update(kind, (old) => old + 1, ifAbsent: () => 1);
    }

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Padding(
            child: Text(
              '${deck.length} remaining tiles',
              style: Theme.of(context).textTheme.headline5,
              textAlign: TextAlign.start,
            ),
            padding: const EdgeInsets.all(8.0),
          ),
          Expanded(
            child: LayoutBuilder(builder: (context, constraints) {
              final crossAxisCount = (constraints.maxWidth / 128).ceil();

              return GridView.count(
                crossAxisCount: crossAxisCount,
                children: TileKind.values
                    .map((kind) => _tile(kind, tileCounts[kind] ?? 0))
                    .toList(),
              );
            }),
          ),
        ],
        crossAxisAlignment: CrossAxisAlignment.stretch,
      ),
    );
  }

  Widget _tile(TileKind kind, int count) {
    return Padding(
      padding: EdgeInsets.all(8),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        child: Stack(
          children: [
            Center(
              child: Image(
                image: kind.asset(),
                filterQuality: defaultFilterQuality,
              ).greyscaled(count == 0),
            ),
            Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [Colors.transparent, Colors.black.withOpacity(0.5)],
                  begin: Alignment.center,
                  end: Alignment.bottomRight,
                ),
              ),
            ),
            Positioned(
              child: Text(
                '$count',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              ),
              right: 4,
              bottom: 4,
            ),
          ],
        ),
      ),
    );
  }
}

class HeroDialogRoute<T> extends PageRoute<T> {
  HeroDialogRoute({
    required WidgetBuilder builder,
    RouteSettings? settings,
    bool fullscreenDialog = false,
  })  : _builder = builder,
        super(settings: settings, fullscreenDialog: fullscreenDialog);

  final WidgetBuilder _builder;

  @override
  bool get opaque => false;

  @override
  bool get barrierDismissible => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 200);

  @override
  bool get maintainState => true;

  @override
  Color get barrierColor => Colors.black54;

  @override
  Widget buildTransitions(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {
    return child;
  }

  @override
  Widget buildPage(
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
  ) {
    return _builder(context);
  }

  @override
  String get barrierLabel => 'Popup dialog open';
}

class GameplayPageArguments {
  final int roomId;
  final Player me;
  final String token;

  GameplayPageArguments({
    required this.roomId,
    required this.me,
    required this.token,
  });
}

extension MaybeWrappedWidget on Widget {
  Widget dimmed(bool dimmed) {
    if (dimmed) {
      return ColorFiltered(
        colorFilter: ColorFilter.mode(Colors.grey.shade400, BlendMode.modulate),
        child: this,
      );
    } else {
      return this;
    }
  }

  Widget greyscaled(bool greyscaled) {
    if (greyscaled) {
      return ColorFiltered(
        colorFilter: ColorFilter.mode(Colors.grey, BlendMode.saturation),
        child: this,
      );
    } else {
      return this;
    }
  }

  Widget tooltipped(String? message, {double? verticalOffset = 48}) {
    if (message != null) {
      return Tooltip(
        message: message,
        child: this,
        verticalOffset: verticalOffset,
      );
    } else {
      return this;
    }
  }
}

extension TileKindAsset on TileKind {
  AssetImage asset() {
    return AssetImage('assets/tiles/${this.name}.png');
  }
}
