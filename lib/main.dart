import 'package:flutter/material.dart';

import './palette.dart';
import './pages/gameplay_page.dart';
import './pages/gameover_page.dart';
import './pages/cover_page.dart';
import './game_utils.dart';

// import 'package:sliding_up_panel/sliding_up_panel.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await TileDataRegistry.initialize();
  print('Initialized tile data');

  runApp(App());
}

class App extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Carcassonne',
      theme: Palette.defaultThemeData,
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        CoverPage.routeName: (context) => CoverPage(),
        GameoverPage.routeName: (context) => GameoverPage(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == GameplayPage.routeName) {
          final args = settings.arguments as GameplayPageArguments;

          return MaterialPageRoute(
            builder: (context) => GameplayPage(args: args),
          );
        } else if (settings.name == JoinRoomPage.routeName) {
          final args = settings.arguments as JoinRoomPageArguments;
          return MaterialPageRoute(
            builder: (context) => JoinRoomPage(args),
          );
        } else {
          throw Exception('No route named ${settings.name}');
        }
      },
    );
  }
}
