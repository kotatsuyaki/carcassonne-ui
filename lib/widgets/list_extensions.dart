import 'package:flutter/material.dart';

extension ListPaddingWidget on List<Widget> {
  List<Widget> mapPadding(EdgeInsetsGeometry padding) {
    return this
        .map((widget) => Padding(padding: padding, child: widget))
        .toList();
  }
}
