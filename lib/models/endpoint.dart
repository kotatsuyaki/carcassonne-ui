String endpointUrl() {
  final url = const String.fromEnvironment(
    'ENDPOINT_URL',
    defaultValue: 'akitaki.ml',
  );
  print('Using endpoint url $url');
  return url;
}

int endpointPort() {
  final portStr = const String.fromEnvironment(
    'ENDPOINT_PORT',
    defaultValue: '38181',
  );

  final port = int.tryParse(portStr);
  if (port != null) {
    print('Using endpoint port $port');
    return port;
  } else {
    throw Exception('Failed to parse ENDPOINT_PORT value "$portStr"');
  }
}
