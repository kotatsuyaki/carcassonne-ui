///
//  Generated code. Do not modify.
//  source: carcassonne.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use rotationDescriptor instead')
const Rotation$json = const {
  '1': 'Rotation',
  '2': const [
    const {'1': 'R0', '2': 0},
    const {'1': 'R90', '2': 1},
    const {'1': 'R180', '2': 2},
    const {'1': 'R270', '2': 3},
  ],
};

/// Descriptor for `Rotation`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List rotationDescriptor = $convert.base64Decode('CghSb3RhdGlvbhIGCgJSMBAAEgcKA1I5MBABEggKBFIxODAQAhIICgRSMjcwEAM=');
@$core.Deprecated('Use tileKindDescriptor instead')
const TileKind$json = const {
  '1': 'TileKind',
  '2': const [
    const {'1': 'CastleCenterPennant', '2': 0},
    const {'1': 'CastleCenterEntry', '2': 1},
    const {'1': 'CastleCenterSide', '2': 2},
    const {'1': 'CastleEdge', '2': 3},
    const {'1': 'CastleEdgeRoad', '2': 4},
    const {'1': 'CastleSides', '2': 5},
    const {'1': 'CastleSidesEdge', '2': 6},
    const {'1': 'CastleTube', '2': 7},
    const {'1': 'CastleWall', '2': 8},
    const {'1': 'CastleWallCurveLeft', '2': 9},
    const {'1': 'CastleWallCurveRight', '2': 10},
    const {'1': 'CastleWallJunction', '2': 11},
    const {'1': 'CastleWallRoad', '2': 12},
    const {'1': 'Cloister', '2': 13},
    const {'1': 'CloisterRoad', '2': 14},
    const {'1': 'Road', '2': 15},
    const {'1': 'RoadCurve', '2': 16},
    const {'1': 'RoadJunctionLarge', '2': 17},
    const {'1': 'RoadJunctionSmall', '2': 18},
    const {'1': 'CastleCenterEntryPennant', '2': 19},
    const {'1': 'CastleCenterSidePennant', '2': 20},
    const {'1': 'CastleEdgePennant', '2': 21},
    const {'1': 'CastleEdgeRoadPennant', '2': 22},
    const {'1': 'CastleTubePennant', '2': 23},
  ],
};

/// Descriptor for `TileKind`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List tileKindDescriptor = $convert.base64Decode('CghUaWxlS2luZBIXChNDYXN0bGVDZW50ZXJQZW5uYW50EAASFQoRQ2FzdGxlQ2VudGVyRW50cnkQARIUChBDYXN0bGVDZW50ZXJTaWRlEAISDgoKQ2FzdGxlRWRnZRADEhIKDkNhc3RsZUVkZ2VSb2FkEAQSDwoLQ2FzdGxlU2lkZXMQBRITCg9DYXN0bGVTaWRlc0VkZ2UQBhIOCgpDYXN0bGVUdWJlEAcSDgoKQ2FzdGxlV2FsbBAIEhcKE0Nhc3RsZVdhbGxDdXJ2ZUxlZnQQCRIYChRDYXN0bGVXYWxsQ3VydmVSaWdodBAKEhYKEkNhc3RsZVdhbGxKdW5jdGlvbhALEhIKDkNhc3RsZVdhbGxSb2FkEAwSDAoIQ2xvaXN0ZXIQDRIQCgxDbG9pc3RlclJvYWQQDhIICgRSb2FkEA8SDQoJUm9hZEN1cnZlEBASFQoRUm9hZEp1bmN0aW9uTGFyZ2UQERIVChFSb2FkSnVuY3Rpb25TbWFsbBASEhwKGENhc3RsZUNlbnRlckVudHJ5UGVubmFudBATEhsKF0Nhc3RsZUNlbnRlclNpZGVQZW5uYW50EBQSFQoRQ2FzdGxlRWRnZVBlbm5hbnQQFRIZChVDYXN0bGVFZGdlUm9hZFBlbm5hbnQQFhIVChFDYXN0bGVUdWJlUGVubmFudBAX');
@$core.Deprecated('Use playerDescriptor instead')
const Player$json = const {
  '1': 'Player',
  '2': const [
    const {'1': 'Red', '2': 0},
    const {'1': 'Blue', '2': 1},
  ],
};

/// Descriptor for `Player`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List playerDescriptor = $convert.base64Decode('CgZQbGF5ZXISBwoDUmVkEAASCAoEQmx1ZRAB');
@$core.Deprecated('Use createRoomRequestDescriptor instead')
const CreateRoomRequest$json = const {
  '1': 'CreateRoomRequest',
  '2': const [
    const {'1': 'withNpc', '3': 1, '4': 1, '5': 8, '10': 'withNpc'},
  ],
};

/// Descriptor for `CreateRoomRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoomRequestDescriptor = $convert.base64Decode('ChFDcmVhdGVSb29tUmVxdWVzdBIYCgd3aXRoTnBjGAEgASgIUgd3aXRoTnBj');
@$core.Deprecated('Use createRoomResponseDescriptor instead')
const CreateRoomResponse$json = const {
  '1': 'CreateRoomResponse',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 13, '10': 'roomId'},
  ],
};

/// Descriptor for `CreateRoomResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createRoomResponseDescriptor = $convert.base64Decode('ChJDcmVhdGVSb29tUmVzcG9uc2USFgoGcm9vbUlkGAEgASgNUgZyb29tSWQ=');
@$core.Deprecated('Use joinRoomRequestDescriptor instead')
const JoinRoomRequest$json = const {
  '1': 'JoinRoomRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 13, '10': 'roomId'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
  ],
};

/// Descriptor for `JoinRoomRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinRoomRequestDescriptor = $convert.base64Decode('Cg9Kb2luUm9vbVJlcXVlc3QSFgoGcm9vbUlkGAEgASgNUgZyb29tSWQSEgoEbmFtZRgCIAEoCVIEbmFtZQ==');
@$core.Deprecated('Use joinRoomResponseDescriptor instead')
const JoinRoomResponse$json = const {
  '1': 'JoinRoomResponse',
  '2': const [
    const {'1': 'me', '3': 1, '4': 1, '5': 14, '6': '.carcassonne.Player', '10': 'me'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
  ],
};

/// Descriptor for `JoinRoomResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinRoomResponseDescriptor = $convert.base64Decode('ChBKb2luUm9vbVJlc3BvbnNlEiMKAm1lGAEgASgOMhMuY2FyY2Fzc29ubmUuUGxheWVyUgJtZRIUCgV0b2tlbhgCIAEoCVIFdG9rZW4=');
@$core.Deprecated('Use gameStateRequestDescriptor instead')
const GameStateRequest$json = const {
  '1': 'GameStateRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 13, '10': 'roomId'},
    const {'1': 'lastSerial', '3': 2, '4': 1, '5': 13, '10': 'lastSerial'},
  ],
};

/// Descriptor for `GameStateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gameStateRequestDescriptor = $convert.base64Decode('ChBHYW1lU3RhdGVSZXF1ZXN0EhYKBnJvb21JZBgBIAEoDVIGcm9vbUlkEh4KCmxhc3RTZXJpYWwYAiABKA1SCmxhc3RTZXJpYWw=');
@$core.Deprecated('Use gameStateResponseDescriptor instead')
const GameStateResponse$json = const {
  '1': 'GameStateResponse',
  '2': const [
    const {'1': 'serial', '3': 1, '4': 1, '5': 13, '10': 'serial'},
    const {'1': 'started', '3': 2, '4': 1, '5': 8, '10': 'started'},
    const {'1': 'board', '3': 3, '4': 1, '5': 11, '6': '.carcassonne.Board', '10': 'board'},
    const {'1': 'numPlayers', '3': 4, '4': 1, '5': 13, '10': 'numPlayers'},
    const {'1': 'legalActions', '3': 5, '4': 3, '5': 11, '6': '.carcassonne.Action', '10': 'legalActions'},
    const {'1': 'playerNames', '3': 6, '4': 3, '5': 9, '10': 'playerNames'},
  ],
};

/// Descriptor for `GameStateResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List gameStateResponseDescriptor = $convert.base64Decode('ChFHYW1lU3RhdGVSZXNwb25zZRIWCgZzZXJpYWwYASABKA1SBnNlcmlhbBIYCgdzdGFydGVkGAIgASgIUgdzdGFydGVkEigKBWJvYXJkGAMgASgLMhIuY2FyY2Fzc29ubmUuQm9hcmRSBWJvYXJkEh4KCm51bVBsYXllcnMYBCABKA1SCm51bVBsYXllcnMSNwoMbGVnYWxBY3Rpb25zGAUgAygLMhMuY2FyY2Fzc29ubmUuQWN0aW9uUgxsZWdhbEFjdGlvbnMSIAoLcGxheWVyTmFtZXMYBiADKAlSC3BsYXllck5hbWVz');
@$core.Deprecated('Use doActionRequestDescriptor instead')
const DoActionRequest$json = const {
  '1': 'DoActionRequest',
  '2': const [
    const {'1': 'roomId', '3': 1, '4': 1, '5': 13, '10': 'roomId'},
    const {'1': 'token', '3': 2, '4': 1, '5': 9, '10': 'token'},
    const {'1': 'action', '3': 3, '4': 1, '5': 11, '6': '.carcassonne.Action', '10': 'action'},
  ],
};

/// Descriptor for `DoActionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List doActionRequestDescriptor = $convert.base64Decode('Cg9Eb0FjdGlvblJlcXVlc3QSFgoGcm9vbUlkGAEgASgNUgZyb29tSWQSFAoFdG9rZW4YAiABKAlSBXRva2VuEisKBmFjdGlvbhgDIAEoCzITLmNhcmNhc3Nvbm5lLkFjdGlvblIGYWN0aW9u');
@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = const {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode('CgVFbXB0eQ==');
@$core.Deprecated('Use boardDescriptor instead')
const Board$json = const {
  '1': 'Board',
  '2': const [
    const {'1': 'tiles', '3': 1, '4': 3, '5': 11, '6': '.carcassonne.TileEntry', '10': 'tiles'},
    const {'1': 'deck', '3': 2, '4': 3, '5': 14, '6': '.carcassonne.TileKind', '10': 'deck'},
    const {'1': 'turn', '3': 3, '4': 1, '5': 14, '6': '.carcassonne.Player', '10': 'turn'},
    const {'1': 'scores', '3': 4, '4': 3, '5': 13, '10': 'scores'},
    const {'1': 'remainingMeeples', '3': 5, '4': 3, '5': 13, '10': 'remainingMeeples'},
  ],
};

/// Descriptor for `Board`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List boardDescriptor = $convert.base64Decode('CgVCb2FyZBIsCgV0aWxlcxgBIAMoCzIWLmNhcmNhc3Nvbm5lLlRpbGVFbnRyeVIFdGlsZXMSKQoEZGVjaxgCIAMoDjIVLmNhcmNhc3Nvbm5lLlRpbGVLaW5kUgRkZWNrEicKBHR1cm4YAyABKA4yEy5jYXJjYXNzb25uZS5QbGF5ZXJSBHR1cm4SFgoGc2NvcmVzGAQgAygNUgZzY29yZXMSKgoQcmVtYWluaW5nTWVlcGxlcxgFIAMoDVIQcmVtYWluaW5nTWVlcGxlcw==');
@$core.Deprecated('Use tileEntryDescriptor instead')
const TileEntry$json = const {
  '1': 'TileEntry',
  '2': const [
    const {'1': 'coord', '3': 1, '4': 1, '5': 11, '6': '.carcassonne.Coord', '10': 'coord'},
    const {'1': 'tile', '3': 2, '4': 1, '5': 11, '6': '.carcassonne.Tile', '10': 'tile'},
    const {'1': 'meeple', '3': 3, '4': 1, '5': 11, '6': '.carcassonne.Meeple', '9': 0, '10': 'meeple', '17': true},
  ],
  '8': const [
    const {'1': '_meeple'},
  ],
};

/// Descriptor for `TileEntry`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tileEntryDescriptor = $convert.base64Decode('CglUaWxlRW50cnkSKAoFY29vcmQYASABKAsyEi5jYXJjYXNzb25uZS5Db29yZFIFY29vcmQSJQoEdGlsZRgCIAEoCzIRLmNhcmNhc3Nvbm5lLlRpbGVSBHRpbGUSMAoGbWVlcGxlGAMgASgLMhMuY2FyY2Fzc29ubmUuTWVlcGxlSABSBm1lZXBsZYgBAUIJCgdfbWVlcGxl');
@$core.Deprecated('Use coordDescriptor instead')
const Coord$json = const {
  '1': 'Coord',
  '2': const [
    const {'1': 'i', '3': 1, '4': 1, '5': 5, '10': 'i'},
    const {'1': 'j', '3': 2, '4': 1, '5': 5, '10': 'j'},
  ],
};

/// Descriptor for `Coord`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List coordDescriptor = $convert.base64Decode('CgVDb29yZBIMCgFpGAEgASgFUgFpEgwKAWoYAiABKAVSAWo=');
@$core.Deprecated('Use tileDescriptor instead')
const Tile$json = const {
  '1': 'Tile',
  '2': const [
    const {'1': 'kind', '3': 1, '4': 1, '5': 14, '6': '.carcassonne.TileKind', '10': 'kind'},
    const {'1': 'rotation', '3': 2, '4': 1, '5': 14, '6': '.carcassonne.Rotation', '10': 'rotation'},
    const {'1': 'meeple_on_feature', '3': 3, '4': 1, '5': 13, '9': 0, '10': 'meepleOnFeature', '17': true},
    const {'1': 'meeple_on_cloister', '3': 4, '4': 1, '5': 13, '9': 1, '10': 'meepleOnCloister', '17': true},
  ],
  '8': const [
    const {'1': '_meeple_on_feature'},
    const {'1': '_meeple_on_cloister'},
  ],
};

/// Descriptor for `Tile`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List tileDescriptor = $convert.base64Decode('CgRUaWxlEikKBGtpbmQYASABKA4yFS5jYXJjYXNzb25uZS5UaWxlS2luZFIEa2luZBIxCghyb3RhdGlvbhgCIAEoDjIVLmNhcmNhc3Nvbm5lLlJvdGF0aW9uUghyb3RhdGlvbhIvChFtZWVwbGVfb25fZmVhdHVyZRgDIAEoDUgAUg9tZWVwbGVPbkZlYXR1cmWIAQESMQoSbWVlcGxlX29uX2Nsb2lzdGVyGAQgASgNSAFSEG1lZXBsZU9uQ2xvaXN0ZXKIAQFCFAoSX21lZXBsZV9vbl9mZWF0dXJlQhUKE19tZWVwbGVfb25fY2xvaXN0ZXI=');
@$core.Deprecated('Use actionDescriptor instead')
const Action$json = const {
  '1': 'Action',
  '2': const [
    const {'1': 'coord', '3': 1, '4': 1, '5': 11, '6': '.carcassonne.Coord', '10': 'coord'},
    const {'1': 'rotation', '3': 2, '4': 1, '5': 14, '6': '.carcassonne.Rotation', '10': 'rotation'},
    const {'1': 'meeple_action', '3': 3, '4': 1, '5': 11, '6': '.carcassonne.MeepleAction', '10': 'meepleAction'},
  ],
};

/// Descriptor for `Action`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List actionDescriptor = $convert.base64Decode('CgZBY3Rpb24SKAoFY29vcmQYASABKAsyEi5jYXJjYXNzb25uZS5Db29yZFIFY29vcmQSMQoIcm90YXRpb24YAiABKA4yFS5jYXJjYXNzb25uZS5Sb3RhdGlvblIIcm90YXRpb24SPgoNbWVlcGxlX2FjdGlvbhgDIAEoCzIZLmNhcmNhc3Nvbm5lLk1lZXBsZUFjdGlvblIMbWVlcGxlQWN0aW9u');
@$core.Deprecated('Use meepleActionDescriptor instead')
const MeepleAction$json = const {
  '1': 'MeepleAction',
  '2': const [
    const {'1': 'none', '3': 1, '4': 1, '5': 8, '9': 0, '10': 'none'},
    const {'1': 'cloister', '3': 2, '4': 1, '5': 8, '9': 0, '10': 'cloister'},
    const {'1': 'edge', '3': 3, '4': 1, '5': 13, '9': 0, '10': 'edge'},
  ],
  '8': const [
    const {'1': 'meepleAction'},
  ],
};

/// Descriptor for `MeepleAction`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List meepleActionDescriptor = $convert.base64Decode('CgxNZWVwbGVBY3Rpb24SFAoEbm9uZRgBIAEoCEgAUgRub25lEhwKCGNsb2lzdGVyGAIgASgISABSCGNsb2lzdGVyEhQKBGVkZ2UYAyABKA1IAFIEZWRnZUIOCgxtZWVwbGVBY3Rpb24=');
@$core.Deprecated('Use meepleDescriptor instead')
const Meeple$json = const {
  '1': 'Meeple',
  '2': const [
    const {'1': 'meeple_action', '3': 1, '4': 1, '5': 11, '6': '.carcassonne.MeepleAction', '10': 'meepleAction'},
    const {'1': 'player', '3': 2, '4': 1, '5': 14, '6': '.carcassonne.Player', '10': 'player'},
  ],
};

/// Descriptor for `Meeple`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List meepleDescriptor = $convert.base64Decode('CgZNZWVwbGUSPgoNbWVlcGxlX2FjdGlvbhgBIAEoCzIZLmNhcmNhc3Nvbm5lLk1lZXBsZUFjdGlvblIMbWVlcGxlQWN0aW9uEisKBnBsYXllchgCIAEoDjITLmNhcmNhc3Nvbm5lLlBsYXllclIGcGxheWVy');
