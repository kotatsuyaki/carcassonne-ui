///
//  Generated code. Do not modify.
//  source: carcassonne.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Rotation extends $pb.ProtobufEnum {
  static const Rotation R0 = Rotation._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'R0');
  static const Rotation R90 = Rotation._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'R90');
  static const Rotation R180 = Rotation._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'R180');
  static const Rotation R270 = Rotation._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'R270');

  static const $core.List<Rotation> values = <Rotation> [
    R0,
    R90,
    R180,
    R270,
  ];

  static final $core.Map<$core.int, Rotation> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Rotation? valueOf($core.int value) => _byValue[value];

  const Rotation._($core.int v, $core.String n) : super(v, n);
}

class TileKind extends $pb.ProtobufEnum {
  static const TileKind CastleCenterPennant = TileKind._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleCenterPennant');
  static const TileKind CastleCenterEntry = TileKind._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleCenterEntry');
  static const TileKind CastleCenterSide = TileKind._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleCenterSide');
  static const TileKind CastleEdge = TileKind._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleEdge');
  static const TileKind CastleEdgeRoad = TileKind._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleEdgeRoad');
  static const TileKind CastleSides = TileKind._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleSides');
  static const TileKind CastleSidesEdge = TileKind._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleSidesEdge');
  static const TileKind CastleTube = TileKind._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleTube');
  static const TileKind CastleWall = TileKind._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleWall');
  static const TileKind CastleWallCurveLeft = TileKind._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleWallCurveLeft');
  static const TileKind CastleWallCurveRight = TileKind._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleWallCurveRight');
  static const TileKind CastleWallJunction = TileKind._(11, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleWallJunction');
  static const TileKind CastleWallRoad = TileKind._(12, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleWallRoad');
  static const TileKind Cloister = TileKind._(13, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Cloister');
  static const TileKind CloisterRoad = TileKind._(14, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CloisterRoad');
  static const TileKind Road = TileKind._(15, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Road');
  static const TileKind RoadCurve = TileKind._(16, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'RoadCurve');
  static const TileKind RoadJunctionLarge = TileKind._(17, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'RoadJunctionLarge');
  static const TileKind RoadJunctionSmall = TileKind._(18, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'RoadJunctionSmall');
  static const TileKind CastleCenterEntryPennant = TileKind._(19, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleCenterEntryPennant');
  static const TileKind CastleCenterSidePennant = TileKind._(20, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleCenterSidePennant');
  static const TileKind CastleEdgePennant = TileKind._(21, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleEdgePennant');
  static const TileKind CastleEdgeRoadPennant = TileKind._(22, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleEdgeRoadPennant');
  static const TileKind CastleTubePennant = TileKind._(23, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CastleTubePennant');

  static const $core.List<TileKind> values = <TileKind> [
    CastleCenterPennant,
    CastleCenterEntry,
    CastleCenterSide,
    CastleEdge,
    CastleEdgeRoad,
    CastleSides,
    CastleSidesEdge,
    CastleTube,
    CastleWall,
    CastleWallCurveLeft,
    CastleWallCurveRight,
    CastleWallJunction,
    CastleWallRoad,
    Cloister,
    CloisterRoad,
    Road,
    RoadCurve,
    RoadJunctionLarge,
    RoadJunctionSmall,
    CastleCenterEntryPennant,
    CastleCenterSidePennant,
    CastleEdgePennant,
    CastleEdgeRoadPennant,
    CastleTubePennant,
  ];

  static final $core.Map<$core.int, TileKind> _byValue = $pb.ProtobufEnum.initByValue(values);
  static TileKind? valueOf($core.int value) => _byValue[value];

  const TileKind._($core.int v, $core.String n) : super(v, n);
}

class Player extends $pb.ProtobufEnum {
  static const Player Red = Player._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Red');
  static const Player Blue = Player._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Blue');

  static const $core.List<Player> values = <Player> [
    Red,
    Blue,
  ];

  static final $core.Map<$core.int, Player> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Player? valueOf($core.int value) => _byValue[value];

  const Player._($core.int v, $core.String n) : super(v, n);
}

