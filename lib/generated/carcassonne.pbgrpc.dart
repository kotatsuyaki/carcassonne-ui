///
//  Generated code. Do not modify.
//  source: carcassonne.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'carcassonne.pb.dart' as $0;
export 'carcassonne.pb.dart';

class GameServiceClient extends $grpc.Client {
  static final _$createRoom =
      $grpc.ClientMethod<$0.CreateRoomRequest, $0.CreateRoomResponse>(
          '/carcassonne.GameService/createRoom',
          ($0.CreateRoomRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.CreateRoomResponse.fromBuffer(value));
  static final _$joinRoom =
      $grpc.ClientMethod<$0.JoinRoomRequest, $0.JoinRoomResponse>(
          '/carcassonne.GameService/joinRoom',
          ($0.JoinRoomRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.JoinRoomResponse.fromBuffer(value));
  static final _$gameState =
      $grpc.ClientMethod<$0.GameStateRequest, $0.GameStateResponse>(
          '/carcassonne.GameService/gameState',
          ($0.GameStateRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.GameStateResponse.fromBuffer(value));
  static final _$doAction = $grpc.ClientMethod<$0.DoActionRequest, $0.Empty>(
      '/carcassonne.GameService/doAction',
      ($0.DoActionRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Empty.fromBuffer(value));

  GameServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$0.CreateRoomResponse> createRoom(
      $0.CreateRoomRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$createRoom, request, options: options);
  }

  $grpc.ResponseFuture<$0.JoinRoomResponse> joinRoom($0.JoinRoomRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$joinRoom, request, options: options);
  }

  $grpc.ResponseFuture<$0.GameStateResponse> gameState(
      $0.GameStateRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$gameState, request, options: options);
  }

  $grpc.ResponseFuture<$0.Empty> doAction($0.DoActionRequest request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$doAction, request, options: options);
  }
}

abstract class GameServiceBase extends $grpc.Service {
  $core.String get $name => 'carcassonne.GameService';

  GameServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.CreateRoomRequest, $0.CreateRoomResponse>(
        'createRoom',
        createRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.CreateRoomRequest.fromBuffer(value),
        ($0.CreateRoomResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.JoinRoomRequest, $0.JoinRoomResponse>(
        'joinRoom',
        joinRoom_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.JoinRoomRequest.fromBuffer(value),
        ($0.JoinRoomResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.GameStateRequest, $0.GameStateResponse>(
        'gameState',
        gameState_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.GameStateRequest.fromBuffer(value),
        ($0.GameStateResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DoActionRequest, $0.Empty>(
        'doAction',
        doAction_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DoActionRequest.fromBuffer(value),
        ($0.Empty value) => value.writeToBuffer()));
  }

  $async.Future<$0.CreateRoomResponse> createRoom_Pre($grpc.ServiceCall call,
      $async.Future<$0.CreateRoomRequest> request) async {
    return createRoom(call, await request);
  }

  $async.Future<$0.JoinRoomResponse> joinRoom_Pre(
      $grpc.ServiceCall call, $async.Future<$0.JoinRoomRequest> request) async {
    return joinRoom(call, await request);
  }

  $async.Future<$0.GameStateResponse> gameState_Pre($grpc.ServiceCall call,
      $async.Future<$0.GameStateRequest> request) async {
    return gameState(call, await request);
  }

  $async.Future<$0.Empty> doAction_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DoActionRequest> request) async {
    return doAction(call, await request);
  }

  $async.Future<$0.CreateRoomResponse> createRoom(
      $grpc.ServiceCall call, $0.CreateRoomRequest request);
  $async.Future<$0.JoinRoomResponse> joinRoom(
      $grpc.ServiceCall call, $0.JoinRoomRequest request);
  $async.Future<$0.GameStateResponse> gameState(
      $grpc.ServiceCall call, $0.GameStateRequest request);
  $async.Future<$0.Empty> doAction(
      $grpc.ServiceCall call, $0.DoActionRequest request);
}
