{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    protobuf

    flutter
    dart
    nodejs
    cmake
    ninja
    clang
    pkg-config
    libselinux
    gtk3.dev
    pcre
    util-linux
    wayland
    libglvnd
    libsepol
    libthai
    libdatrie
    xorg.libX11
    xorg.libXdmcp
  ];
  shellHook = ''
    export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${pkgs.wayland}/lib:${pkgs.libglvnd}/lib:${pkgs.xorg.libX11}/lib
    export FLUTTER_SDK=${pkgs.flutter.unwrapped}
  '';
  CHROME_EXECUTABLE = "${pkgs.chromium}/bin/chromium";
}

